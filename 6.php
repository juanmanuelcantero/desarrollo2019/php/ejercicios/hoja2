<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>6</title>
    </head>
    <body>
        <?php
        if ($mal) {
            ?>
        <form name="f">
            <legend>
                Selecciona la ciudad
            </legend>
            <input type="checkbox" name="ciudad[]" id="SS" value="SS" />
            <label for="SS">Santander</label>
            <input type="checkbox" name="ciudad[]" id="PO" value="PO" />
            <label for="PO">Potes</label>
            <input type="checkbox" name="ciudad[]" id="PA" value="PA" />
            <label for="PA">Palencia</label>
            <input type="submit" value="Enviar" name="boton"/>
        </form>
        <?php
        } else {
            $ciudades = array(
                "SS"=>"Santander",
                "PO"=>"Potes",
                "PA"=>"Palencia"
            );
            echo "los elementos seleccionados son: ";
            foreach ($_GET['ciudad'] as $value) {
                echo "<br>$value-$ciudades[$value]";
            }
        }
        ?>
    </body>
</html>
