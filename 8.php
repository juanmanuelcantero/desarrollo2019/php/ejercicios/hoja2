<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if ($mal) {
            ?>
        <form name="f">
            <legend>
                Selecciona la ciudad
            </legend>
            <input type="radio" name="ciudad" id="SS" value="SS" />
            <label for="SS">Santander</label>
            <input type="radio" name="ciudad" id="PO" value="PO" />
            <label for="PO">Potes</label>
            <input type="radio" name="ciudad" id="PA" value="PA" />
            <label for="PA">Palencia</label>
            <input type="submit" value="Enviar" name="boton"/>
        </form>
        <?php
        } else {
            $ciudades = [
                "SS"=>"Santander",
                "PO"=>"Potes",
                "PA"=>"Palencia"
            ];
            echo "el elemento seleccionado es: ";
            echo $ciudades[$_REQUEST['ciudad']];
            }
        ?>
    </body>
</html>
